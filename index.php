<?php
session_start();
?>
<html>
<head>
<title>Renewal report email</title>
<link rel="icon" href="https://www.fwd.co.th/-/media/global/images/fwdlogod.svg" type="image/gif">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<?php $DM = date("m")+2; ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">IMPORT RENEWAL REPORT EMAILL (NonMotor MOUNT<?php echo $DM;?>)</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          SELECT TYPE UPLOAD
        </a>
        <?php include("menu.php"); ?>
      </li>
    </ul>
  </div>
</nav>
<?php
include ("inc/connect_fwdgi2.php");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/src/SimpleXLSX.php';
require_once __DIR__.'/src/SimpleXLSXGen.php';
include("../include/misc.php");
ini_set('memory_limit', '3048M');
ini_set('max_execution_time',52000);
if(@$_GET['cancle'] == 'Y') {
	echo '<meta http-equiv="refresh" content="0;URL=index.php">';
}
if(!file_exists(''.$DM.'_Renewal.xlsx')) {
?>
	<table width="100%" border="1" class="table table-bordered">
		<tr>
			<td colspan="8">
				<form action="index.php" method="post" enctype="multipart/form-data">
					<div><label for="exampleFormControlFile1">Please upload file excel(.xlsx)</label></div>
					<span class="badge badge-warning">1</span>
					<input type="file" name="fileToUpload" id="fileToUpload">
					<span class="badge badge-warning">2</span>
					<input type="submit" value="Upload Image" class="btn btn-secondary btn-md" name="submit">
				</form>
				<div><a href="Renewal_sample.xlsx">Download Sample File(xlsx)</a></div>
			</td>
		</tr>
		<?php
		 if(@$_GET['done'] == 'Y') {
		?>
		<tr>
			<td colspan="3">
				COMPLETE !!
			</td>
		</tr>
		<?php
		echo '<meta http-equiv="refresh" content="1;URL=index.php">';
		}
		?>
	</table>
<?php
	if(@$_FILES["fileToUpload"]["name"]) {
		$_SESSION['FileExpire'] = 'Y';
		move_uploaded_file($_FILES['fileToUpload']['tmp_name'], "".$DM."_Renewal.xlsx");
		echo '<meta http-equiv="refresh" content="0;URL=index.php">';
	}
}
else {
	if(empty($_SESSION['FileExpire'])) {
		unlink(''.$DM.'_Renewal.xlsx');
		if (file_exists(''.$DM.'_Renewal_.xlsx')) {
			unlink(''.$DM.'_Renewal_.xlsx');
		}
		echo '<meta http-equiv="refresh" content="0;URL=index.php">';

	}
	if ($xlsx = SimpleXLSX::parse(''.$DM.'_Renewal.xlsx')) {
		$dealer = array();
		$header_values = $rows = [];

		foreach ( $xlsx->rows() as $k => $r ) {
			if ( $k === 0 ) {
				$header_values = $r;
				continue;
			}
			$rows[] = array_combine($header_values,$r);
		}

	    foreach ($rows as  $value) {
	       // echo  $value['รหัสตัวแทน'];
	       // echo '<br>';
	    	if(!empty($value["รหัสตัวแทน"])) {
	       		array_push($dealer, $value["รหัสตัวแทน"]);
	       }

	    }
	    @$unique = array_unique($dealer);
		@$agentId = "'".implode("','",$unique)."'";
	}
	if(!@$_POST['SearchAgentId']) {
		@$sql = "SELECT * FROM MAS_AGENT WHERE AGENT_ID IN($agentId)";
	}
	else {
		@$sql = "SELECT * FROM MAS_AGENT WHERE AGENT_ID IN('".$_POST['SearchAgentId']."')";
	}
	//
	$query = mysqli_query($conn,$sql);
	$query1 = mysqli_query($conn,$sql);

	$arrayrowscount = array();
	while (@$rowsCount = mysqli_fetch_array($query1,MYSQLI_ASSOC)) {
	    array_push($arrayrowscount, $rowsCount['AGENT_ID']);
	}
	?>
	<table width="100%" border="1" class="table table-bordered">
		<tr>
		<td>
			<form action="index.php" method="post">
				<input type="hidden" value="Confirmemail" name="action">
				<input type="submit" class="btn btn-info btn-sm" value="SEND" name="submit">
			</form>
		</td>
		<td colspan="3">
			<form action="index.php" method="post" id="SampleTestSend_submit">
				<div class="input-group mb-3">
				  <input type="text" class="form-control" name="SearchAgentId" placeholder="Please input AGENT iD" aria-label="Please input AGENT iD" aria-describedby="button-addon2" value="<?php echo @$_POST['SearchAgentId'];?>">
				  <div class="input-group-append">
				    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">SERACH</button>
				   <button class="btn btn-outline-secondary"> <a href="index.php?done=Y">RESET</a></button>
				  </div>
				</div>
			</form>
		</td>
		</tr>
		<tr>
		<td class="align-middle">
		TOTAL READ: <?php echo count(@$rows);?> ROW
		<div>DUPLICATE : <?php echo count(@$unique); ?> ROW</div>
		<div>Total EMAIL SEND : <?php echo count($arrayrowscount); ?> EMAIL</div>
		</td>
		<td colspan="9" class="align-middle">
		<span class="badge badge-warning">Please upload file excel(.xlsx)</span>
		<form action="index.php" method="post" enctype="multipart/form-data">
			<input type="hidden" value="UPLOAD" name="action">
			<input type="file" name="fileToUpload" id="fileToUpload">
			<input type="submit" class="btn btn-secondary btn-sm" value="Upload Image" name="submit">
		</form>
		<p><a href="Renewal_sample.xlsx">Download Sample File(xlsx)</a> | <a href="index.php?clear=Y">[Clear list table]</a></p>
		</td>
		</tr>
		<?PHP
		if(@$_POST['action'] == 'Confirmemail') {
		?>
		<tr>
			<td colspan="10">
				<center>
				<ul class="list-inline">
				  <li class="list-inline-item">
				  	<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="EMAIL" name="typename">
						<input type="submit" class="btn btn-info btn-sm" value="SENT EMAIL" name="submit">
					</form>
				  </li>
				  <li class="list-inline-item">
				  	<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="SMS" name="typename">
						<input type="submit" class="btn btn-info btn-sm" value="SENT SMS" name="submit">
						<a href="index.php?cancle=Y"><input type="button" class="btn btn-danger btn-sm" value="CANCLE" name="submit"></a>
				    </form>
				  </li>

				</ul>
				</center>
			</td>
		</tr>
	    <?php } ?>
	    <?php if(@$_POST['SearchAgentId']) { ?>
	    <tr>
	    	<td colspan="5">
	    		<CENTER>
					<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
						<input type="hidden" value="email" name="action_sample">
						<input type="submit" class="btn btn-primary  btn-lg btn-block" value="TEST SEND (EMAIL)" name="submit">
					</form>
			</td>
			<td colspan="5">
					<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
						<input type="hidden" value="sms" name="action_sample">
						<input type="text" class="form-control" name="anphone">
						<div class="pt-2">
						<input type="submit" class="btn btn-primary  btn-lg btn-block" value="TEST SEND (SMS)" name="submit">
					    </div>
					</form>
				</CENTER>
	    	</td>
	    </tr>
	     <tr>
	    	<td colspan="5">
	    		<CENTER>
					<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
						<input type="hidden" value="emailfix" name="action_sample">
						<input type="submit" class="btn btn-success  btn-lg btn-block" value="SEND (EMAIL) AGIAN" name="submit">
					</form>
			</td>
			<td colspan="5">
					<form action="index.php" method="post">
						<input type="hidden" value="ConfirmSendmail" name="action">
						<input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
						<input type="hidden" value="smsfix" name="action_sample">
						<input type="submit" class="btn btn-success  btn-lg btn-block" value="SEND (SMS) AGIAN" name="submit">
					</form>
				</CENTER>
	    	</td>
	    </tr>

	    <?php } ?>
	    <?php
	    if(@$_GET['clear'] == 'Y') {
	    ?>
		<tr>
			<td colspan="6">
				<CENTER>
				<form action="index.php" method="post">
					<input type="hidden" value="Delete" name="action">
					<input type="submit" class="btn btn-info btn-sm" value="CONFIRM CLEAR LIST TALBE" name="submit">
					<a href="index.php?cancle=Y"><input type="button" class="btn btn-danger btn-sm" value="CANCLE" name="submit"></a>
				</form>
				</CENTER>
			</td>
		</tr>
		<?php } ?>
		<?php
		if(@$_POST['action'] == 'ConfirmSendmail') {
		?>
		<tr>
		<td colspan="3"><center>SEND EMAIL DONE</center></td>
		</tr>
		<?php
		}
		?>
		<tr>
			<td><b>No</b></td>
			<td><b>รหัสตัวแทน</b></td>
			<td><b>ตัวแทน</b></td>
			<td><b><center>Email</center></b></td>
			<td><b><center>Phone</center></b></td>
			<td><b>Date start</b></td>
			<td><b>Date expire</b></td>
			<td><b>File</b></td>
			<td><b>Sent Mail</b></td>
			<td><b>SMS</b></td>
		</tr>
		<?php
			$RowNumber = 1;
			$showlist = 0;
			$SmsNotUse = array ('8200626','6400008','8200650','9001109','9001110','6200037','8200757','6200023','9001099','6200001','6200040','6200021','6300016','6200022','6200006','9001013','6300020','6200036','9001088','6200008','6200039','6300009','6400040','6400004','9001107','6300024','8200524','6300022','6300018','9001081','6300012','9001084','6300003','6300001','4102003','6400032','6300011','6200038','9001032','9001077','6400009','6400005','9001024','6200033','8200655','9001021','9001056','6200003','6400026','9001101','3101002','3101001','6300014','8200198','8200593','6400016','6400021','6400038','9001091','9001047','8200239','6400017','8200017','6400007','8200265','8001009','9001096','8200668','9001076','6300017','9001112','8010048','8010053','6200002','6400010','6300002','6300025','6200032','6300026');
		    $SmsDirect = array('9102248','4102002','4101001','6200005','6300021','4102001','6200015','6200017','6200024','6300004','6200014','8200104','8200153','8200165','8200205','8200211','6200001');
		    $SmsPartner = array('9001106','6300019','6300023');
			$numphone  = array('053','043','081','082','083','084','085','086','087','088','089','091','092','093','094','095','096','097','098','099');
			$showlist = 0;
			while(@$result=mysqli_fetch_array($query,MYSQLI_ASSOC))  {
				//
				$Array_Folder_Upload = array();
				$Array_Folder_Filename = array();
				$Dir_Upload = "upload/nonmotor/";
				if (is_dir($Dir_Upload)){
				  if ($Dh_Upload = opendir($Dir_Upload)){
				    while (($File_Upload = readdir($Dh_Upload)) !== false){
				      array_push($Array_Folder_Upload, iconv_substr($File_Upload,0,10));
				      array_push($Array_Folder_Filename, $File_Upload);
				    }
				    closedir($Dh_Upload);
				  }
				}
				$Array_Folder_Upload_key =  array_search($DM.'_'.$result['AGENT_ID'], $Array_Folder_Upload);
			    $excelfile =  $Array_Folder_Filename[$Array_Folder_Upload_key];
				//
				$Check_phone = substr($result['AGENT_PHONE'], 0,3);
					if (!in_array($result['AGENT_ID'],$SmsNotUse) ) {
							$books = [
								['วันที่สิ้นสุด','วันที่เริ่มต้น','รหัสแผนกฝ่ายผู้ดูแล','แผนกฝ่ายผู้ดูแล','รหัสผู้ดูแล','ผู้ดูแล','รหัสตัวแทน','ตัวแทน','รหัสสาขา','ชื่อสาขา','รหัสประเภทธุรกิจ','ประเภทธุรกิจ','กธ.เลขที่/สลักหลังเลขที่','ประเภทสลักหลัง','คำนำหน้าชื่อผอป.','ชื่อผอป.','นามสกุลผอป.','จำนวน Location ทั้งหมดของกธ','ทุนประกัน','สถานะ','เบี้ยสุทธิ','รหัสภัย','ส่วนลดลูกค้า','เลขที่เคลม','INCURR','LOSS RATIO']
							];
							echo '<tr>';

							echo '</tr>';
							$phonenum = 0;
							foreach ($rows as $resultall) {
								if($result['AGENT_ID'] == $resultall["รหัสตัวแทน"]) {
									$phonenum++;
									@$Policy_No = $resultall['กธ.เลขที่/สลักหลังเลขที่'];
									$Product_Type = $resultall['ประเภทธุรกิจ'];
									// echo '<tr>';
									// echo '<td>'.$resultall["รหัสตัวแทน"].'</td>';
									// echo '<td>'.$resultall["ผู้ดูแล"].'</td>';
									// echo '<td>'.$result["AGENT_MAIL"].'</td>';
									// echo '<td>YES</td>';
									// echo '</tr>';
									$startdate = $resultall["วันที่เริ่มต้น"];
									$expire =  $resultall["วันที่สิ้นสุด"];
									// if($phonenum == 1) {
									// 		@$exphone = trim(str_replace('-', '', $resultall["MT_INS_MOBILE_NO"]));
									// 	}
									@$exphone =  $result['AGENT_PHONE'];
									array_push($books, array($resultall["วันที่สิ้นสุด"],$resultall["วันที่เริ่มต้น"],$resultall["รหัสแผนกฝ่ายผู้ดูแล"],$resultall["แผนกฝ่ายผู้ดูแล"],$resultall["รหัสผู้ดูแล"],$resultall["ผู้ดูแล"],$resultall["รหัสตัวแทน"],$resultall["ตัวแทน"],$resultall["รหัสสาขา"],$resultall["ชื่อสาขา"],$resultall["รหัสประเภทธุรกิจ"],$resultall["ประเภทธุรกิจ"],@$resultall["กธ.เลขที่/สลักหลังเลขที่"],$resultall["ประเภทสลักหลัง"],$resultall["คำนำหน้าชื่อผอป."],$resultall["ชื่อผอป."],$resultall["นามสกุลผอป."],$resultall["จำนวน Location ทั้งหมดของกธ"],$resultall["ทุนประกัน"],$resultall["สถานะ"],$resultall["เบี้ยสุทธิ"],$resultall["รหัสภัย"],$resultall["ส่วนลดลูกค้า"],$resultall["เลขที่เคลม"],$resultall["INCURR"],$resultall["LOSS RATIO"]));
								}
							}
							//
							echo '<tr>';
							echo '<td>'.$RowNumber++.'</td>';
							echo '<td>'.$result['AGENT_ID'].'</td>';
							echo '<td>'.$result['AGENT_NAME'].'</td>';
							echo '<td><center>'.$result['AGENT_MAIL'].'</center></td>';
							echo '<td><center>'.$exphone.'</center></td>';
							echo '<td>'.date("d-m-Y",strtotime($startdate)).'</td>';
							echo '<td>'.date("d-m-Y",strtotime($expire)).'</td>';
							echo '<td><a href="upload/nonmotor/'.$excelfile.'">'.$excelfile.'</a></td>';
							if (file_exists(''.$DM.'_Renewal_.xlsx')) {
								echo '<td><span class="badge badge-success">YES</span></td>';
								echo '<td><span class="badge badge-success">YES</span></td>';
							}
							else {
								echo '<td><span class="badge badge-warning">NO</span></td>';
								echo '<td><span class="badge badge-warning">NO</span></td>';
							}
							echo '</tr>';
							//
							 if(@$_POST['action'] == 'ConfirmSendmail') {
							 	 copy(''.$DM.'_Renewal.xlsx', ''.$DM.'_Renewal_.xlsx');
								 $filenameExcel = 'upload/nonmotor/'.$DM.'_'.$result['AGENT_ID'].'_'.date("dmy").'.xlsx';
								 $xlsx = SimpleXLSXGen::fromArray( $books ,'รายงานกรมธรรม์หมดอายุ(INM3206)');
							  	 $xlsx->saveAs($filenameExcel);
							  	 $books=array();
								 // //
								 $from = 'policy-support@fwdgi.co.th';
								 //$to = 'prasit.dee@fwdgi.com';
								 $to = trim($result['AGENT_MAIL']);

								 //$to = 'Tm.auttanop@gmail.com';
								 $subject = '=?utf-8?B?'.base64_encode("รายงานกรมธรรม์ที่จะสิ้นสุดในเดือน".date("m/Y", strtotime($expire))."").'?=';
								 include("email_template.php");
								 $message = $EmailTemplate;
								 $arrFiles = array(@$filename,$filenameExcel);
								 //
								 if(@$_POST['action_sample'] == 'sms' || @$_POST['action_sample'] == 'smsfix') {
								 	//SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
								 }
								 else if(@$_POST['action_sample'] == 'email') {
								   SendMail_MultiAttach($from, 'prasit.dee@fwdgi.com', $subject, $message, $arrFiles);
								 }
								 else if(@$_POST['action_sample'] == 'emailfix') {
								   SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
								 }
								 else {
								 	if($_POST['typename'] == 'EMAIL') {
								 		  SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
										  $randomstr=rand();
										  $randomcode = md5($randomstr);

										  $sql_check_email_log   ="SELECT * FROM RENEWAL_LOG_ES WHERE RE_AGENT_ID='".$result['AGENT_ID']."' AND RE_MOUNT='".$DM."' AND RE_TYPE='NON_MOTOR'";
										  $sql_check_email_query = mysqli_query($conn,$sql_check_email_log);
										  $sql_check_email_row   = mysqli_fetch_array($sql_check_email_query,MYSQLI_ASSOC);

										  if($sql_check_email_row['RE_AGENT_ID']) {
										  	 $updte_check_email = "UPDATE RENEWAL_LOG_ES SET RE_EMAIL='".'Email_'.$result['AGENT_MAIL']."' WHERE RE_AGENT_ID='".$result['AGENT_ID']."' AND RE_MOUNT='".$DM."' AND RE_TYPE='NON_MOTOR'";
										  	 mysqli_query($conn,$updte_check_email);

										  }
										  else {
											  $RE_AGENT_ID = $result['AGENT_ID'];
											  $RE_POLICY_NO = $Policy_No;
											  $RE_GROUP = $Log_Type;
											  $RE_TYPE = 'NON_MOTOR';
											  $RE_AG = '0';
											  $RE_VP = '0';
											  $RE_AGP = '0';
											  $RE_SMS = '';
											  $RE_EMAIL = 'Email_'.$result['AGENT_MAIL'];
											  $RE_CODE = $randomcode;
											  $RE_CODE_ACTIVE = '0';
											  $RE_IPADDRESS = '0';
											  $sql = "INSERT INTO RENEWAL_LOG_ES (RE_AGENT_ID,RE_POLICY_NO,RE_GROUP,RE_TYPE,RE_AG,RE_VP,RE_AGP,RE_SMS,RE_EMAIL,RE_CODE,RE_CODE_ACTIVE,RE_IPADDRESS,RE_MOUNT,RE_STARTDATE,RE_LASTDATE)VALUES ('".$RE_AGENT_ID."','".$RE_POLICY_NO."','".$RE_GROUP."','".$RE_TYPE."','".$RE_AG."','".$RE_VP."','".$RE_AGP."','".$RE_SMS."','".$RE_EMAIL."','".$RE_CODE."','".$RE_CODE_ACTIVE."','".$RE_IPADDRESS."'
												,'".$DM."','".date("Y-m-d h:i:s")."','".date("Y-m-d h:i:s")."')";
												mysqli_query($conn,$sql);
										}
									}
								 }

							    //------ /SEND SMS TO CUSTOMER ---- //
								if(in_array($result['AGENT_ID'], $SmsDirect)) {
									$text_message = "".$Product_Type." เลขที่ ".$Policy_No." กำลังจะหมดความคุ้มครองในวันที่ ".date("d/m/Y", strtotime($expire))." ".$result['AGENT_PHONE']."";
									$Log_Type = 'DIRECT';
								}
								else if(in_array($result['AGENT_ID'], $SmsPartner)) {
									$text_message = "".$Product_Type." เลขที่ ".$Policy_No." กำลังจะหมดความคุ้มครองในวันที่ ".date("d/m/Y", strtotime($expire))." Call center / Form";
									$Log_Type = 'PARTNER';
								}else {
									$text_message = "".$Product_Type." เลขที่ ".$Policy_No." กำลังจะหมดความคุ้มครองในวันที่ ".date("d/m/Y", strtotime($expire))."  กรุณาติดต่อ ".$result['AGENT_PHONE']."";
									$Log_Type = 'AGENT';
								}
								// Function Send SMS //
								error_reporting(E_ALL);
								//$Username	= "test@siamcity";
								$ch = curl_init();
								if(@$_POST['action_sample'] == 'email' || @$_POST['action_sample'] == 'emailfix') {
								   $Username		= "admin*siam";
							       $Password		= "SiamCity";
							       //$PhoneList		= $result['AGENT_PHONE'];
							       $PhoneList		= '0958307222';
								   $Message		    = urlencode($text_message);
								   //$Sender		= "SiamCityIns";
							       $Sender			= "FWDGI";

							       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
								   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
								 	//actiontest   curl_setopt($ch,CURLOPT_URL,$API_URL);
									//actiontest   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
									//actiontest   curl_setopt($ch,CURLOPT_POST,1);
									//actiontest   curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

									//actiontest $Result = curl_exec($ch);
									//actiontest curl_close($ch);
								 }
								 else if(@$_POST['action_sample'] == 'sms') {
							 	   $Username		= "admin*siam";
							       $Password		= "SiamCity";
							       //$PhoneList		= $result['AGENT_PHONE'];
							       if ($_POST['anphone']) {
							       		$PhoneList  = $_POST['anphone'];
							       }
							       else {
							       	    $PhoneList  = '0958307222';
							       }
							       $Message		    = urlencode($text_message);
								   //$Sender		= "SiamCityIns";
							       $Sender			= "FWDGI";

							       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
								   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
								    curl_setopt($ch,CURLOPT_URL,$API_URL);
								    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
								    curl_setopt($ch,CURLOPT_POST,1);
								    curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

								    $Result = curl_exec($ch);
								    curl_close($ch);
								 }
								  else if(@$_POST['action_sample'] == 'smsfix') {
							 	   $Username		= "admin*siam";
							       $Password		= "SiamCity";
							       $PhoneList		= $exphone;
							       //$PhoneList		= '0958307222';
								   $Message		    = urlencode($text_message);
								   //$Sender		= "SiamCityIns";
							       $Sender			= "FWDGI";

							       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
								   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
								    curl_setopt($ch,CURLOPT_URL,$API_URL);
								    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
								    curl_setopt($ch,CURLOPT_POST,1);
								    curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

								    $Result = curl_exec($ch);
								    curl_close($ch);
								 }
								 else {
								 	if($_POST['typename'] == 'SMS') {
								   		   $Username		= "admin*siam";
									       $Password		= "SiamCity";
									       $PhoneList		= $exphone;
									       //$PhoneList		= '0958307222';
										   $Message		    = urlencode($text_message);
										   //$Sender		= "SiamCityIns";
									       $Sender			= "FWDGI";
									   if(in_array(substr($PhoneList, 0,3), $numphone) && $PhoneList !='') {
										       $Parameter =	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
											   $API_URL	= "http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
											    curl_setopt($ch,CURLOPT_URL,$API_URL);
											    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
											    curl_setopt($ch,CURLOPT_POST,1);
											    curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

											    $Result = curl_exec($ch);
											    curl_close($ch);

											    $randomstr=rand();
											    $randomcode = md5($randomstr);

												$sql_check_email_log   ="SELECT * FROM RENEWAL_LOG_ES WHERE RE_AGENT_ID='".$result['AGENT_ID']."' AND RE_MOUNT='".$DM."' AND RE_TYPE='NON_MOTOR'";
												$sql_check_email_query = mysqli_query($conn,$sql_check_email_log);
												$sql_check_email_row   = mysqli_fetch_array($sql_check_email_query,MYSQLI_ASSOC);

												if($sql_check_email_row['RE_AGENT_ID']) {
												$updte_check_email = "UPDATE RENEWAL_LOG_ES SET RE_SMS='".'SMS_'.$PhoneList."' WHERE RE_AGENT_ID='".$result['AGENT_ID']."' AND RE_MOUNT='".$DM."' AND RE_TYPE='NON_MOTOR'";
												mysqli_query($conn,$updte_check_email);

												}
												else {
													$RE_AGENT_ID = $result['AGENT_ID'];
													$RE_POLICY_NO = $Policy_No;
													$RE_GROUP = $Log_Type;
													$RE_TYPE = 'NON_MOTOR';
													$RE_AG = '0';
													$RE_VP = '0';
													$RE_AGP = '0';
													$RE_SMS = 'SMS_'.$PhoneList;
													$RE_EMAIL = '';
													$RE_CODE = $randomcode;
													$RE_CODE_ACTIVE = '0';
													$RE_IPADDRESS = '0';
													$Re_mount = $DM;
													$sql = "INSERT INTO RENEWAL_LOG_ES (RE_AGENT_ID,RE_POLICY_NO,RE_GROUP,RE_TYPE,RE_AG,RE_VP,RE_AGP,RE_SMS,RE_EMAIL,RE_CODE,RE_CODE_ACTIVE,RE_IPADDRESS,RE_MOUNT,RE_STARTDATE,RE_LASTDATE)VALUES ('".$RE_AGENT_ID."','".$RE_POLICY_NO."','".$RE_GROUP."','".$RE_TYPE."','".$RE_AG."','".$RE_VP."','".$RE_AGP."','".$RE_SMS."','".$RE_EMAIL."','".$RE_CODE."','".$RE_CODE_ACTIVE."','".$RE_IPADDRESS."'
													,'".$DM."','".date("Y-m-d h:i:s")."','".date("Y-m-d h:i:s")."')";
													mysqli_query($conn,$sql);
											    }
									    }
									}
								 }
													//------ /SEND SMS TO CUSTOMER ---- //
								if(@$_POST['action_sample'] == 'email' || @$_POST['action_sample'] == 'sms' || @$_POST['action_sample'] == 'smsfix' || @$_POST['action_sample'] == 'emailfix') {
									echo "<script type='text/javascript'>document.getElementById('SampleTestSend_submit').submit();</script>";
									exit();
							    }
							    echo '<meta http-equiv="refresh" content="0;URL=index.php">';

							 }
					}
			}
			if(@$_POST['action'] == 'Delete') {
				unlink(''.$DM.'_Renewal.xlsx');
				if (file_exists(''.$DM.'_Renewal_.xlsx')) {
					unlink(''.$DM.'_Renewal_.xlsx');
				}
				echo '<meta http-equiv="refresh" content="0;URL=index.php?done=Y">';
			}
		?>
	</table>
<?php
}
?>
</table>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</body>
</html>