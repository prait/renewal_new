<?php
$EmailTemplate ='
<div>
	<div><font color="#E87722" face="Tahoma" size="3">เรียน คุณ '.$result['AGENT_NAME'].' รหัสตัวแทน ['.$result['AGENT_ID'].']</font></div>
	<br>
	<div class="pt-4"><font face="Tahoma" size="2">ทางบริษัทขอนำส่งรายงานกรมธรรม์ที่จะสิ้นสุดในเดือน '.date("m/Y", strtotime($expire)).'</font></div>
	<div><font face="Tahoma" size="2">เพื่อความสะดวกในการ และติดตามกรมธรรมที่กำลังจะมาถึง</font></div>
	<br>
	<br>
	<div class="pt-4"><font face="Tahoma" size="2">อีเมลฉบับนี้เป็นการแจ้งข้อมูลจากระบบโดยอัตโนมัติ กรุณาอย่าตอบกลับ หากท่านมี</font></div>
	<div><font face="Tahoma" size="2">ข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อ</font></div>
	<div><font face="Tahoma" size="2">ฝ่ายงานดูแลตัวแทนของท่าน</font></div>
    <div class="c"></div>
    <br>
    <div class="pt-4"><font face="Tahoma" size="3">ด้วยความเครพอย่างสูง</font></div>
	<br>
	<div><font face="Tahoma" color="#E87722" size="3">บริษัท เอฟดับบลิวดีประกันภัย จำกัด (มหาชน)</font></div>

	<div><img src="https://www.fwd.co.th/-/media/global/images/fwdlogod.svg"></div>
</div>
';

?>





