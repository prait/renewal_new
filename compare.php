<?php
session_start();
?>
<html>
<head>
<title>Renewal report email</title>
<link rel="icon" href="https://www.fwd.co.th/-/media/global/images/fwdlogod.svg" type="image/gif">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-light bg-light">
	<a class="navbar-brand" href="#">
	<H3>COMPARE EMAILL</H3>
	</a>
</nav>
<?php
include ("inc/connect_fwdgi2.php");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/src/SimpleXLSX.php';
require_once __DIR__.'/src/SimpleXLSXGen.php';
include("../include/misc.php");
ini_set('memory_limit', '3048M');
ini_set('max_execution_time',52000);

	if ($xlsx = SimpleXLSX::parse('fwdgi.xlsx')) {
		$dealer = array();
		$header_values = $rows = [];

		foreach ( $xlsx->rows() as $k => $r ) {
			if ( $k === 0 ) {
				$header_values = $r;
				continue;
			}
			$rows[] = array_combine( $header_values, $r );
		}

	    foreach ($rows as  $value) {
	       // echo  $value['รหัสตัวแทน'];
	       // echo '<br>';
	       array_push($dealer, $value["รหัสตัวแทน"]);
	    }
	    $unique = array_unique($dealer);
	    //echo count($unique);
		@$agentId = "'".implode("','",$unique)."'";
	}
	?>
	<table width="100%" border="1" class="table table-bordered">
		<tr>
			<td><b>No</b></td>
			<td><b>รหัสตัวแทน</b></td>
			<td><b>ตัวแทน</b></td>
			<td><b><center>PHONE</center></b></td>
			<td><b><center>Email xlsx</center></b></td>
			<td><b><center>Email database</center></b></td>
			<td><b><center>Phone database</center></b></td>
		</tr>
		<?php
			$i=0;
			$RowNumber = 1;
			foreach ($rows as $resultall) {
						$sql = "SELECT * FROM MAS_AGENT WHERE AGENT_ID ='".$resultall['รหัสตัวแทน']."'";
						$query = mysqli_query($conn,$sql);
						$result=mysqli_fetch_array($query,MYSQLI_ASSOC);
						//
						$i++;
						if(!empty($result['AGENT_ID'])) {
							echo '<tr>';
							echo '<td>'.$i.'</td>';
							echo '<td>'.$resultall["รหัสตัวแทน"].'</td>';
							echo '<td>'.$resultall["ชื่อไทย"].'</td>';
							echo '<td>'.trim(str_replace("-","",$resultall["เบอร์โทร (AGENT)"])).'</td>';
							echo '<td>'.$resultall["E-mail"].'</td>';
							echo '<td>'.$result["AGENT_MAIL"].'</td>';
							echo '<td>'.$result["AGENT_PHONE"].'</td>';
							echo '</tr>';
							if(@$_GET['update'] == 'Y') {

								$InputPhone = $resultall["เบอร์โทร (AGENT)"];
								if(strpos($InputPhone,",")) {
									$SplitPhone  = explode( ',', $InputPhone );
									$FomattPhone = $SplitPhone[1];
								}
								else if(strpos($InputPhone," ")){
									$SplitPhone  = explode(' ', $InputPhone );
									$FomattPhone = $SplitPhone[1];
								}
								else {
									$FomattPhone=$InputPhone;
								}

								//$sql_update  =  "UPDATE MAS_AGENT SET AGENT_MAIL=' ', AGENT_PHONE='".trim(str_replace("-","",$resultall["เบอร์โทร (AGENT)"]))."' WHERE AGENT_ID ='".$result['AGENT_ID']."'";
								//$sql_update  =  "UPDATE MAS_AGENT SET AGENT_MAIL='".$resultall["E-mail"]."', AGENT_PHONE='".trim(str_replace("-","",$FomattPhone))."' WHERE AGENT_ID ='".$result['AGENT_ID']."'";
								$sql_update  =  "UPDATE MAS_AGENT SET AGENT_PHONE='".trim(str_replace("-","",$FomattPhone))."' WHERE AGENT_ID ='".$resultall["รหัสตัวแทน"]."'";
								mysqli_query($conn,$sql_update);
							}
						}
						//
				}
			if(@$_GET['update'] == 'Y') {
				echo '<meta http-equiv="refresh" content="0;URL=compare.php">';
			}

		?>
	</table>
</table>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script></body>
</body>
</html>