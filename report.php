<?php
session_start();
?>
<html>
<head>
<title>Renewal report email</title>
<link rel="icon" href="https://www.fwd.co.th/-/media/global/images/fwdlogod.svg" type="image/gif">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<body>
<div class="container-fulid">

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">IMPORT RENEWAL REPORT</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          SELECT TYPE UPLOAD
        </a>
        <?php include("menu.php"); ?>
      </li>
    </ul>
  </div>
</nav>
<?php
include ("inc/connect_fwdgi2.php");
//
$Filter = array();
$Tbname  =array();
if($_POST['rp_date']) {
	array_push($Filter, "RE_MOUNT=".$_POST['rp_date']."");
}
if($_POST['rp_type']) {
    $rp_type_name ='';
	for($i=0; $i< count($_POST['rp_type']); $i++) {
		$rp_type_name .="'".$_POST['rp_type'][$i]."',";
	}
	array_push($Filter, "RE_TYPE IN(".substr($rp_type_name,0, -1).")");
}
if($_POST['rp_group']) {
	$rp_group_name ='';
	for($j=0; $j< count($_POST['rp_group']); $j++) {
		$rp_group_name .="'".$_POST['rp_group'][$j]."',";
	}
	array_push($Filter, "RE_GROUP IN(".substr($rp_group_name,0, -1).")");
}
$k=-1;
$where ='';
foreach ($Filter as $Filter_value) {
	$k++;
	if($k==0) {
		$where .=' WHERE '.$Filter_value;
	}
	else if($k >0) {
		$where .= ' AND '.$Filter_value;
	}
}
//
$Count_lastdate = array();
$sql_date = "SELECT * FROM RENEWAL_LOG_ES";
$query_date = mysqli_query($conn,$sql_date);
while ($result_date = mysqli_fetch_array($query_date,MYSQLI_ASSOC)) {
	array_push($Count_lastdate, $result_date['RE_MOUNT']);
}
if($where) {
	 $sql ="SELECT * FROM RENEWAL_LOG_ES ".$where."";
} else {
	 $sql ="SELECT * FROM RENEWAL_LOG_ES";
}
//
$query = mysqli_query($conn,$sql);
$query1 = mysqli_query($conn,$sql);
$Count_AgentMotor = array();
$Count_AgentNonmotor = array();
$Count_Agent = array();
$Count_direc = array();
$Count_partfner = array();





$Count_motor_agent = array();
$Count_motor_direc = array();
$Count_motor_partner = array();



$Count_nonmotor_agent = array();
$Count_nonmotor_direc = array();
$Count_nonmotor_partner = array();



while ($result1 = mysqli_fetch_array($query1,MYSQLI_ASSOC)) {
		if($result1['RE_TYPE'] == 'MOTOR')  {
		   array_push($Count_AgentMotor, '1');
		}
		if($result1['RE_TYPE'] == 'NON_MOTOR')  {
		   array_push($Count_AgentNonmotor, '1');
		}
		if($result1['RE_GROUP'] == 'AGENT')  {
		   array_push($Count_Agent, '1');
		}
		if($result1['RE_GROUP'] == 'DIRECT')  {
		   array_push($Count_direc, '1');
		}
		if($result1['RE_GROUP'] == 'PARTNER')  {
		   array_push($Count_partfner, '1');
		}
		//
		if($result1['RE_TYPE'] == 'NON_MOTOR' && $result1['RE_GROUP'] == 'AGENT')  {
		   array_push($Count_nonmotor_agent, '1');
		}
		if($result1['RE_TYPE'] == 'NON_MOTOR' && $result1['RE_GROUP'] == 'DIRECT')  {
		   array_push($Count_nonmotor_direc, '1');
		}
		if($result1['RE_TYPE'] == 'NON_MOTOR' && $result1['RE_GROUP'] == 'PARTNER')  {
		   array_push($Count_nonmotor_partner, '1');
		}
		if($result1['RE_TYPE'] == 'MOTOR' && $result1['RE_GROUP'] == 'AGENT')  {
		   array_push($Count_motor_agent, '1');
		}
		if($result1['RE_TYPE'] == 'MOTOR' && $result1['RE_GROUP'] == 'DIRECT')  {
		   array_push($Count_motor_direc, '1');
		}
		if($result1['RE_TYPE'] == 'MOTOR' && $result1['RE_GROUP'] == 'PARTNER')  {
		   array_push($Count_motor_partner, '1');
		}
		//array_push($Count_lastdate, date("Y-d-m",$result1['RE_LASTDATE']));
		//array_push($Count_lastdate, $result1['RE_LASTDATE']);
}
$Count_lastdate_ = array_unique($Count_lastdate);
?>
<div class="container-fulid pl-3 pr-3 pt-3">
	    <div class="row">
			<div class="col-sm">
				<div class="card border-secondary mb-3">
				  <div class="card-header"><b>MOTOR</b></div>
				  <div class="card-body text-secondary">
				    <h5 class="card-title"><center><span class="text-primary">EMAIL</span> | <span class="text-success">SMS </span>SEND</center></h5>
				    <p class="card-text"><center><h1 style="font-size: 5rem;"><?php echo count($Count_AgentMotor); ?></h1></center><center></p>
				  </div>
				   <div class="card-header"><b>AGENT</b> <span style="float: right;"><?php echo count($Count_motor_agent);?></span></div>
				   <div class="card-header"><b>DIRCE</b> <span style="float: right;"><?php echo count($Count_motor_direc);?></span></div>
				   <div class="card-header"><b>PARTNER</b> <span style="float: right;"><?php echo count($Count_motor_partner);?></span></div>
				</div>
			</div>
			<div class="col-sm">
				<div class="card border-secondary mb-3">
				  <div class="card-header"><b>NON MOTOR</b></div>
				  <div class="card-body text-secondary">
				    <h5 class="card-title"><center><span class="text-primary">EMAIL</span> | <span class="text-success">SMS </span>SEND</center></h5>
				    <p class="card-text"><center><h1 style="font-size: 5rem;"><?php echo count($Count_AgentNonmotor); ?></h1></center><center></p>
				  </div>
				  <div class="card-header"><b>AGENT</b> <span style="float: right;"><?php echo count($Count_nonmotor_agent);?></span></div>
				   <div class="card-header"><b>DIRCE</b> <span style="float: right;"><?php echo count($Count_nonmotor_direc);?></span></div>
				   <div class="card-header"><b>PARTNER</b> <span style="float: right;"><?php echo count($Count_nonmotor_partner);?></span></div>
				</div>
			</div>
			<div class="col-sm">
				<div class="card border-secondary mb-3">
				  <div class="card-header"><b>AGENT TYPE</b></div>
				  <div class="card-body text-secondary">
				    <h5 class="card-title pt-3">AGENT = <?php echo count($Count_Agent); ?></h5>
				    <h5 class="card-title pt-3">DIRECT = <?php echo count($Count_direc); ?></h5>
				    <h5 class="card-title pt-2">PARTNER = <?php echo count($Count_partfner); ?></h5>
				    <p class="card-text"></p>
				  </div>
				</div>
			</div>
			<!--  -->
		</div>
		<div class="row my-4">
			<div class="col-sm">
				  <form action="report.php" method="post">
				  	      <div class="form-group">
						    <label for="exampleFormControlSelect1">FILLTER BY DATE</label>
						    <select name="rp_date" class="form-control" id="exampleFormControlSelect1">
						      <option value="">Please selelct</option>
						      <?php foreach ($Count_lastdate_ as $Count_lastdate_value) { ?>
						      <option value="<?php echo $Count_lastdate_value;?>"><?php echo $Count_lastdate_value;?></option>
						      <?php } ?>
						    </select>
						  </div>
						  <!--  -->
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" name="rp_type[]" id="exampleRadios1" value="MOTOR">
							  <label class="form-check-label" for="exampleRadios1">
							    MOTOR
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" name="rp_type[]" id="exampleRadios2" value="NON_MOTOR">
							  <label class="form-check-label" for="exampleRadios2">
							    NON-MOTOR
							  </label>
							</div>
							<hr>
						  <!--  -->
						  	<div class="form-check">
							  <input class="form-check-input" type="checkbox" name="rp_group[]" id="exampleRadios1" value="AGENT">
							  <label class="form-check-label" for="exampleRadios1">
							    AGENT
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" name="rp_group[]" id="exampleRadios2" value="DIRECT">
							  <label class="form-check-label" for="exampleRadios2">
							    DIRECT
							  </label>
							</div>
							<div class="form-check disabled">
							  <input class="form-check-input" type="checkbox" name="rp_group[]" id="exampleRadios3" value="PARTNER">
							  <label class="form-check-label" for="exampleRadios3">
							    PARTNER
							  </label>
							</div>
							<button type="submit" class="btn btn-primary mt-2">FILTER</button>
					</form>
				  <!--  -->
			</div>
		</div>
		<table id="example" class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
					<th>id</th>
					<th>AGENT_ID</th>
					<th>POLICY_NO</th>
					<th>GROUP</th>
					<th>TYPE</th>
					<th>EMAIL</th>
					<th>PHONE</th>
					<th>S</th>
					<th>M</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?PHP
	        	while ($result = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
	        		$DM = $result['RE_MOUNT'];
	        		$Array_Folder_Upload = array();
					$Array_Folder_Filename = array();
					if($result['RE_TYPE'] == 'NON_MOTOR') {
						$Dir_Upload = "upload/nonmotor/";
					}
					else if($result['RE_TYPE'] == 'MOTOR') {
						$Dir_Upload = "upload/motor/";
					}
					if (is_dir($Dir_Upload)){
					  if ($Dh_Upload = opendir($Dir_Upload)){
					    while (($File_Upload = readdir($Dh_Upload)) !== false){
					      array_push($Array_Folder_Upload, iconv_substr($File_Upload,0,10));
					      array_push($Array_Folder_Filename, $File_Upload);
					    }
					    closedir($Dh_Upload);
					  }
					}
					$Array_Folder_Upload_key =  array_search($DM.'_'.$result['RE_AGENT_ID'], $Array_Folder_Upload);
			    	$excelfile =  $Array_Folder_Filename[$Array_Folder_Upload_key];
	        	?>
	            <tr>
					<td><?php echo $result['id']; ?></td>
					<td><?php echo $result['RE_AGENT_ID']; ?></td>
					<td><?php echo $result['RE_POLICY_NO']; ?></td>
					<td><?php echo $result['RE_GROUP']; ?></td>
					<td><?php echo $result['RE_TYPE']; ?></td>
					<td><?php echo $result['RE_EMAIL']; ?></td>
					<td><?php echo $result['RE_SMS']; ?></td>
					<?php
					 if($result['RE_EMAIL']) { $tdemail = '<span class="badge badge-pill badge-warning">M</span>'; } else { $tdemail=''; }
					 if($result['RE_SMS']) { $tdsms = '<span class="badge badge-pill badge-warning">S</span>'; } else { $tdsms=''; }
					?>
					<td style="cursor: pointer;"><?php echo $tdsms; ?></td>
					<td style="cursor: pointer;"><?php echo $tdemail; ?></td>

	            </tr>
	        	<?php } ?>
	        </tbody>
	        <tfoot>
	            <tr>
	                <th>id</th>
					<th>AGENT_ID</th>
					<th>POLICY_NO</th>
					<th>GROUP</th>
					<th>TYPE</th>
					<th>EMAIL</th>
					<th>PHONE</th>
					<th>S</th>
					<th>M</th>
	            </tr>
	        </tfoot>
	    </table>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="src/datatables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.0/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>