<?php
session_start();
?>
<html>
<head>
<title>Renewal report email</title>
<link rel="icon" href="https://www.fwd.co.th/-/media/global/images/fwdlogod.svg" type="image/gif">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">IMPORT RENEWAL REPORT EMAIL(AG)</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          SELECT TYPE UPLOAD
        </a>
        <?php include("menu.php"); ?>
      </li>
    </ul>
  </div>
</nav>
<?php
include ("inc/connect_fwdgi2.php");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', true);

require_once __DIR__.'/src/SimpleXLSX.php';
require_once __DIR__.'/src/SimpleXLSXGen.php';
include("../include/misc.php");
ini_set('memory_limit', '3048M');
ini_set('max_execution_time',52000);

$DM = date("m")+2;
if(@$_GET['cancle'] == 'Y') {
	echo '<meta http-equiv="refresh" content="0;URL=index_ag.php">';
}
if(!file_exists('Renewal_ag.xlsx')) {
?>
	<table width="100%" border="1" class="table table-bordered">
		<tr>
			<td colspan="8">
				<form action="index_ag.php" method="post" enctype="multipart/form-data">
					<div><label for="exampleFormControlFile1">Please upload file excel(.xlsx)</label></div>
					<span class="badge badge-warning">1</span>
					<input type="file" name="fileToUpload" id="fileToUpload">
					<span class="badge badge-warning">2</span>
					<input type="submit" value="Upload Image" class="btn btn-secondary btn-md" name="submit">
				</form>
				<div><a href="Renewal_sample.xlsx">Download Sample File(xlsx)</a></div>
			</td>
		</tr>
		<?php
		 if(@$_GET['done'] == 'Y') {
		?>
		<tr>
			<td colspan="3">
				    COMPLETE !!
			</td>
		</tr>
		<?php
		echo '<meta http-equiv="refresh" content="1;URL=index_ag.php">';
		}
		?>
	</table>
<?php
	if(@$_FILES["fileToUpload"]["name"]) {
		$_SESSION['FileExpire'] = 'Y';
		move_uploaded_file($_FILES['fileToUpload']['tmp_name'], "Renewal_ag.xlsx");
		echo '<meta http-equiv="refresh" content="0;URL=index_ag.php">';
	}
}
else {
	if(empty($_SESSION['FileExpire'])) {
		unlink('Renewal_ag.xlsx');
		if (file_exists('Renewalag_.xlsx')) {
			unlink('Renewalag_.xlsx');
		}
		echo '<meta http-equiv="refresh" content="0;URL=index_ag.php">';

	}
	if ($xlsx = SimpleXLSX::parse('Renewal_ag.xlsx')) {
		$dealer = array();
		$header_values = $rows = [];

		foreach ( $xlsx->rows() as $k => $r ) {
			if ( $k === 0 ) {
				$header_values = $r;
				continue;
			}
			$rows[] = array_combine($header_values, $r);
		}

	    foreach ($rows as  $value) {
	       // echo  $value['รหัสตัวแทน'];
	       // echo '<br>';
	    	if(!empty($value["รหัสตัวแทน"])) {
	       		array_push($dealer, $value["รหัสตัวแทน"]);
	       }

	    }
	    @$unique = array_unique($dealer);
		@$agentId = "'".implode("','",$unique)."'";

		$sql_lastdate = "SELECT * FROM RENEWAL_LOG_ES  ORDER BY  id DESC";
	    $sql_lastdate_query  = mysqli_query($conn,$sql_lastdate);
	    $result_lastdate =mysqli_fetch_array($sql_lastdate_query,MYSQLI_ASSOC);
	    $lastdate  = $result_lastdate['RE_MOUNT'];
	}
	    if(!@$_POST['SearchAgentId']) {
        @$sql = "SELECT * FROM MAS_AGENT WHERE AGENT_ID IN($agentId)";
    }
    else {
        @$sql = "SELECT * FROM MAS_AGENT WHERE AGENT_ID IN('".$_POST['SearchAgentId']."')";
    }
    //
	$query = mysqli_query($conn,$sql);
	$query1 = mysqli_query($conn,$sql);

	$arrayrowscount = array();
	while (@$rowsCount = mysqli_fetch_array($query1,MYSQLI_ASSOC)) {
	    array_push($arrayrowscount, $rowsCount['AGENT_ID']);
	}

	?>
	<table width="100%" border="1" class="table table-bordered">
		<tr>
		<td>
			<form action="index_ag.php" method="post">
				<input type="hidden" value="Confirmemail" name="action">
				<input type="submit" class="btn btn-info btn-sm" value="SEND MAIL" name="submit">
			</form>
		</td>
		<td colspan="3">
            <form action="index_ag.php" method="post" id="SampleTestSend_submit">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" name="SearchAgentId" placeholder="Please input AGENT iD" aria-label="Please input AGENT iD" aria-describedby="button-addon2" value="<?php echo @$_POST['SearchAgentId'];?>">
                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">SERACH</button>
                   <button class="btn btn-outline-secondary"> <a href="index_ag.php?done=Y">RESET</a></button>
                  </div>
                </div>
            </form>
        </td>
		</tr>
		<tr>
		<td class="align-middle">
		TOTAL READ: <?php echo count(@$rows);?> ROW
		<div>DUPLICATE : <?php echo count(@$unique); ?> ROW</div>
		</td>
		<td colspan="9" class="align-middle">
		<span class="badge badge-warning">Please upload file excel(.xlsx)</span>
		<form action="index_ag.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="UPLOAD" name="action">
		<input type="file" name="fileToUpload" id="fileToUpload">
		<input type="submit" class="btn btn-secondary btn-sm" value="Upload Image" name="submit">
		</form>
		<p><a href="Renewal_sample.xlsx">Download Sample File(xlsx)</a> | <a href="index_ag.php?clear=Y">[Clear list table]</a></p>
		</td>
		</tr>
		<?PHP
		if(@$_POST['action'] == 'Confirmemail') {
		?>
		<tr>
			<td colspan="10">
				<CENTER>
				<form action="index_ag.php" method="post">
					<input type="hidden" value="ConfirmSendmail" name="action">
					<input type="submit" class="btn btn-info btn-sm" value="CONFIRM SENT EMAIL" name="submit">
					<a href="index_ag.php?cancle=Y"><input type="button" class="btn btn-danger btn-sm" value="CANCLE" name="submit"></a>
				</form>
				</CENTER>
			</td>
		</tr>
	    <?php } ?>
	            <?php if(@$_POST['SearchAgentId']) { ?>
        <tr>
            <td colspan="5">
                <CENTER>
                    <form action="index_ag.php" method="post">
                        <input type="hidden" value="ConfirmSendmail" name="action">
                        <input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
                        <input type="hidden" value="email" name="action_sample">
                        <input type="submit" class="btn btn-primary  btn-lg btn-block" value="TEST SEND (EMAIL)" name="submit">
                    </form>
            </td>
            <td colspan="5">
                    <form action="index_ag.php" method="post">
                        <input type="hidden" value="ConfirmSendmail" name="action">
                        <input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
                        <input type="hidden" value="sms" name="action_sample">
                        <input type="submit" class="btn btn-primary  btn-lg btn-block" value="TEST SEND (SMS)" name="submit">
                    </form>
                </CENTER>
            </td>
        </tr>
         <tr>
            <td colspan="5">
                <CENTER>
                    <form action="index_ag.php" method="post">
                        <input type="hidden" value="ConfirmSendmail" name="action">
                        <input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
                        <input type="hidden" value="emailfix" name="action_sample">
                        <input type="submit" class="btn btn-success  btn-lg btn-block" value="SEND (EMAIL) AGIAN" name="submit">
                    </form>
            </td>
            <td colspan="5">
                    <form action="index_ag.php" method="post">
                        <input type="hidden" value="ConfirmSendmail" name="action">
                        <input type="hidden" value="<?php echo $_POST['SearchAgentId'];?>" name="SearchAgentId">
                        <input type="hidden" value="smsfix" name="action_sample">
                        <input type="submit" class="btn btn-success  btn-lg btn-block" value="SEND (SMS) AGIAN" name="submit">
                    </form>
                </CENTER>
            </td>
        </tr>

        <?php } ?>
	    <?php
	    if(@$_GET['clear'] == 'Y') {
	    ?>
		<tr>
			<td colspan="6">
				<CENTER>
				<form action="index_ag.php" method="post">
					<input type="hidden" value="Delete" name="action">
					<input type="submit" class="btn btn-info btn-sm" value="CONFIRM CLEAR LIST TALBE" name="submit">
					<a href="index_ag.php?cancle=Y"><input type="button" class="btn btn-danger btn-sm" value="CANCLE" name="submit"></a>
				</form>
				</CENTER>
			</td>
		</tr>
		<?php } ?>
		<?php
		if(@$_POST['action'] == 'ConfirmSendmail') {
		?>
		<tr>
		<td colspan="3"><center>SEND EMAIL DONE</center></td>
		</tr>
		<?php
		}
		?>
		<tr>
			<td><b>No</b></td>
            <td><b>รหัสตัวแทน</b></td>
            <td><b>ตัวแทน</b></td>
            <td><b><center>Email</center></b></td>
            <td><b><center>Phone</center></b></td>
            <td><b>Date start</b></td>
            <td><b>Date expire</b></td>
            <td><b>File</b></td>
            <td><b>Sent Mail</b></td>
		</tr>
		<?php
			$RowNumber = 1;
			while(@$result=mysqli_fetch_array($query,MYSQLI_ASSOC))  {
				    $Array_Folder_Upload = array();
					$Array_Folder_Filename = array();
					$Dir_Upload = "upload/motor/";
					if (is_dir($Dir_Upload)){
					  if ($Dh_Upload = opendir($Dir_Upload)){
					    while (($File_Upload = readdir($Dh_Upload)) !== false){
					      if(iconv_substr($File_Upload,0,2) == $lastdate) {
					      	 array_push($Array_Folder_Upload, iconv_substr(iconv_substr($File_Upload,0,10), 3));
					      	 array_push($Array_Folder_Filename, $File_Upload);
					      }
					    }
					    closedir($Dh_Upload);
					  }
					}
					//
					@$Array_Folder_Upload_key =  array_search($result['AGENT_ID'], $Array_Folder_Upload);

					if(@iconv_substr($Array_Folder_Filename[$Array_Folder_Upload_key],-4) == 'xlsx') {
							$books = [
							['รหัสตัวแทน','สาขา','รหัสสาขา','คำนำหน้าชื่อ','ชื่อไทย','Agent Position','Agent Position Edit','AL รหัส GI','AL Edit','VP รหัส GI','VP Edit','AGP รหัส GI','AGP Edit','On Board','วันหมดอายุ','วันที่แต่งตั้ง','E-mail','Zone Menager','Zone Support','เบอร์โทร (AGENT)']
							];
							echo '<tr>';

							echo '</tr>';
							foreach ($rows as $resultall) {
								if($result['AGENT_ID'] == $resultall["รหัสตัวแทน"]) {
									$Policy_No = $resultall['รหัสสาขา'];
									$Product_Type = $resultall['Agent Position'];
									$startdate = $resultall["วันที่แต่งตั้ง"];
									$expire = $resultall["วันหมดอายุ"];
									array_push($books, array($resultall["รหัสตัวแทน"],$resultall["สาขา"],$resultall["รหัสสาขา"],$resultall["คำนำหน้าชื่อ"],$resultall["ชื่อไทย"],$resultall["Agent Position"],$resultall["Agent Position Edit"],$resultall["AL รหัส GI"],$resultall["AL Edit"],$resultall["VP รหัส GI"],$resultall["VP Edit"],$resultall["AGP รหัส GI"],$resultall["AGP Edit"],$resultall["On Board"],$resultall["วันหมดอายุ"],$resultall["วันที่แต่งตั้ง"],$resultall["E-mail"],$resultall["Zone Menager"],$resultall["Zone Support"],$resultall["เบอร์โทร (AGENT)"]));
								}
							}
							//
							echo '<tr>';
							echo '<td>'.$RowNumber++.'</td>';
							echo '<td>'.$result['AGENT_ID'].'</td>';
							echo '<td>'.$result['AGENT_NAME'].'</td>';
							echo '<td><center>'.$result['AGENT_MAIL'].'</center></td>';
							echo '<td><center>'.$result['AGENT_PHONE'].'</center></td>';
				            echo '<td>'.date("d-m-Y",strtotime($startdate)).'</td>';
			                echo '<td>'.date("d-m-Y",strtotime($expire)).'</td>';
			                if($Array_Folder_Upload_key) {
			                    echo '<td><a href="upload/motor/nonmotor/'.$Array_Folder_Filename[$Array_Folder_Upload_key].'">'.$Array_Folder_Filename[$Array_Folder_Upload_key].'</a></td>';
			                }
			                else {
			               	   echo '<td></td>';
			                }
							if (file_exists('Renewalag_.xlsx')) {
								echo '<td><span class="badge badge-success">YES</span></td>';
							}
							else {
								echo '<td><span class="badge badge-warning">NO</span></td>';
							}
							echo '</tr>';
							//
							 	if(@$_POST['action'] == 'ConfirmSendmail') {
					 			 	 //copy('Renewal_ag.xlsx', 'Renewalag_.xlsx');
								 	 // fwd
										 $filenameExcel = 'upload/'.$Array_Folder_Filename[$Array_Folder_Upload_key].'';
										 //$xlsx = SimpleXLSXGen::fromArray( $books ,'รายงานกรมธรรม์หมดอายุ(INM3206)');
									     //$xlsx->saveAs($filenameExcel);
									     //$books=array();
								  	 // fwd
									 // //
									 $from = 'policy-support@fwdgi.co.th';
									 //$to = 'prasit.dee@fwdgi.com';
									 $to = $result['AGENT_MAIL'];
									 //$to = 'Tm.auttanop@gmail.com';
									 $subject = '=?utf-8?B?'.base64_encode("รายงานกรมธรรม์ที่จะสิ้นสุดในเดือน".date("m/Y", strtotime($expire))."").'?=';
									 include("email_template.php");
									 $message = $EmailTemplate;
									 $arrFiles = array(@$filename,$filenameExcel);
									 //
							         if(@$_POST['action_sample'] == 'sms' || @$_POST['action_sample'] == 'smsfix') {
			                             //SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
			                         }
			                         else if(@$_POST['action_sample'] == 'email') {
			                             SendMail_MultiAttach($from, 'prasit.dee@fwdgi.com', $subject, $message, $arrFiles);
			                         }
			                         else if(@$_POST['action_sample'] == 'emailfix') {
			                             SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
			                         }
			                         else {
			                            SendMail_MultiAttach($from, $to, $subject, $message, $arrFiles);
										$randomstr=rand();
										$randomcode = md5($randomstr);

										$RE_AGENT_ID = $result['AGENT_ID'];
										$RE_POLICY_NO = $Policy_No;
										$RE_GROUP = '';
										$RE_TYPE = 'AG';
										$RE_AG = '1';
										$RE_VP = '0';
										$RE_AGP = '0';
										$RE_SMS = $result['AGENT_PHONE'];
										$RE_EMAIL = $result['AGENT_MAIL'];
										$RE_CODE = $randomcode;
										$RE_CODE_ACTIVE = '0';
										$RE_IPADDRESS = '0';
										$sql = "INSERT INTO RENEWAL_LOG_ES (RE_AGENT_ID,RE_POLICY_NO,RE_GROUP,RE_TYPE,RE_AG,RE_VP,RE_AGP,RE_SMS,RE_EMAIL,RE_CODE,RE_CODE_ACTIVE,RE_IPADDRESS,RE_MOUNT,RE_STARTDATE,RE_LASTDATE)VALUES ('".$RE_AGENT_ID."','".$RE_POLICY_NO."','".$RE_GROUP."','".$RE_TYPE."','".$RE_AG."','".$RE_VP."','".$RE_AGP."','".$RE_SMS."','".$RE_EMAIL."','".$RE_CODE."','".$RE_CODE_ACTIVE."','".$RE_IPADDRESS."'
										,'".$DM."','".date("Y-m-d h:i:s")."','".date("Y-m-d h:i:s")."')";
			                         }

									 //------ /SEND SMS TO CUSTOMER ---- //
									$text_message = "".$Product_Type." เลขที่ ".$Policy_No." กำลังจะหมดความคุ้มครองในวันที่ ".date("d/m/Y", strtotime($expire))." กรุณาติดต่อฝ่ายบริการลูกค้า 02-123-4567  บมจ. เอฟดับบลิวดีประกันภัย";http://203.150.67.29/sciagent/renewal/index_ag.php?clear=Y

									// Function Send SMS //
									error_reporting(E_ALL);
									$ch = curl_init();
									if(@$_POST['action_sample'] == 'email' || @$_POST['action_sample'] == 'emailfix') {
									   $Username		= "admin*siam";
								       $Password		= "SiamCity";
								       //$PhoneList		= $result['AGENT_PHONE'];
								       $PhoneList		= '0958307222';
									   $Message		    = urlencode($text_message);
									   //$Sender		= "SiamCityIns";
								       $Sender			= "FWDGI";

								       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
									   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
									 	//actiontest   curl_setopt($ch,CURLOPT_URL,$API_URL);
										//actiontest   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
										//actiontest   curl_setopt($ch,CURLOPT_POST,1);
										//actiontest   curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

										//actiontest $Result = curl_exec($ch);
										//actiontest curl_close($ch);
									 }
									 else if(@$_POST['action_sample'] == 'sms') {
								 	   $Username		= "admin*siam";
								       $Password		= "SiamCity";
								       //$PhoneList		= $result['AGENT_PHONE'];
								       $PhoneList		= '0958307222';
									   $Message		    = urlencode($text_message);
									   //$Sender		= "SiamCityIns";
								       $Sender			= "FWDGI";

								       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
									   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
									   curl_setopt($ch,CURLOPT_URL,$API_URL);
									   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
									   curl_setopt($ch,CURLOPT_POST,1);
									   curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

									   $Result = curl_exec($ch);
									   curl_close($ch);
									 }
									  else if(@$_POST['action_sample'] == 'smsfix') {
								 	   $Username		= "admin*siam";
								       $Password		= "SiamCity";
								       $PhoneList		= $result['AGENT_PHONE'];
								       //$PhoneList		= '0958307222';
									   $Message		    = urlencode($text_message);
									   //$Sender		= "SiamCityIns";
								       $Sender			= "FWDGI";

								       $Parameter		=	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
									   $API_URL		=	"http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
									   curl_setopt($ch,CURLOPT_URL,$API_URL);
									   curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
									   curl_setopt($ch,CURLOPT_POST,1);
									   curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

									   $Result = curl_exec($ch);
									   curl_close($ch);
									 }
									 else {
									   $Username		= "admin*siam";
								       $Password		= "SiamCity";
								       $PhoneList		= $result['AGENT_PHONE'];
								       //$PhoneList		= '0958307222';
									   $Message		    = urlencode($text_message);
									   //$Sender		= "SiamCityIns";
								       $Sender			= "FWDGI";

								       $Parameter =	"User=$Username&Password=$Password&Msnlist=$PhoneList&Msg=$Message&Sender=$Sender";
									   $API_URL	= "http://piesoft.smsmkt.com/SMSLink/SendMsg/index.php";
									   // curl_setopt($ch,CURLOPT_URL,$API_URL);
									   // curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
									   // curl_setopt($ch,CURLOPT_POST,1);
									   // curl_setopt($ch,CURLOPT_POSTFIELDS,$Parameter);

									   // $Result = curl_exec($ch);
									   // curl_close($ch);
									 }
									//------ /SEND SMS TO CUSTOMER ---- //
									if(@$_POST['action_sample'] == 'email' || @$_POST['action_sample'] == 'sms' || @$_POST['action_sample'] == 'smsfix' || @$_POST['action_sample'] == 'emailfix') {
										echo "<script type='text/javascript'>document.getElementById('SampleTestSend_submit').submit();</script>";
										exit();
								    }
								    else {
								    	mysqli_query($conn,$sql);
								    }
								    if(@$_POST['action_sample'] == 'smsfix' || @$_POST['action_sample'] == 'emailfix') {
								    	mysqli_query($conn,$sql);
								    }
								    echo '<meta http-equiv="refresh" content="0;URL=index_ag.php">';

							    }
					}
			}
			if(@$_POST['action'] == 'Delete') {
				unlink('Renewal_ag.xlsx');
				if (file_exists('Renewalag_.xlsx')) {
					unlink('Renewalag_.xlsx');
				}
				echo '<meta http-equiv="refresh" content="0;URL=index_ag.php?done=Y">';
			}
		?>
	</table>
<?php
}
?>
</table>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script></body>
</body>
</html>